package com.xurmo.code.unittests;

import com.xurmo.code.MultiStack;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;

/**
 * @author Ashwin
 */
public class MultiStackTest {
    private MultiStack multiStack;

    @Before
    public void setUp(){
        this.multiStack = new MultiStack(1000,4);
    }


    @Test
    public void testPushPop(){
        multiStack.push(1,"foo");
        multiStack.push(2,"foo1");
        multiStack.push(3,"foo2");
        multiStack.pop(1);
        multiStack.push(1,"foo3");
        multiStack.push(1,"foo4");
        multiStack.push(2,"foo5");
        multiStack.push(2,"foo6");
        multiStack.push(3,"foo7");
        multiStack.push(4,"bar");


        Assert.assertThat((String)multiStack.pop(4),is("bar"));
        Assert.assertThat((String)multiStack.pop(1),is("foo4"));
        Assert.assertThat((String)multiStack.pop(2),is("foo6"));
        Assert.assertThat((String)multiStack.pop(1),is("foo3"));
        Assert.assertThat((String)multiStack.pop(2),is("foo5"));
        Assert.assertThat((String)multiStack.pop(3),is("foo7"));
        Assert.assertThat((String)multiStack.pop(3),is("foo2"));
    }

    @Test
    public void testLimits(){
        this.multiStack = new MultiStack(10,2);
          for(int i = 0;i<10;i++){
              multiStack.push(1,"foo");
          }

        Assert.assertThat(multiStack.getSizeForStack(1),is(10));
        Assert.assertThat(multiStack.getSizeForStack(2),is(0));
        multiStack.pop(1);
        multiStack.pop(1);
        multiStack.push(2,"bar");
        multiStack.push(2,"bar");
        Assert.assertThat(multiStack.getSizeForStack(1),is(8));
        Assert.assertThat(multiStack.getSizeForStack(2),is(2));
    }


}
