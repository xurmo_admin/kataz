package com.xurmo.code;

/**
 * @author Ashwin
 */
public class MultiStack {

    private int capacity;
    
    private Entry[] data;

    private XStack[] stacks;



    public MultiStack(int capacity,int numberOfStacks) {
        this.capacity = capacity;
        this.data = new Entry[capacity];
        this.stacks = new XStack[numberOfStacks];
        init();
    }

    private void init() {
        for(int i = 0;i < stacks.length; i++){
            stacks[i] = new XStack();
        }

        for (int i = 0;i < data.length; i++){
            data[i] = new Entry();
        }


    }

    public int getSizeForStack(int stackNo){
        return this.stacks[stackNo-1].getSize();
    }

    public void push(int stackNumber, Object data) {
        Entry entry = new Entry();
        entry.setValue(data);
        entry.setStackNumber(stackNumber);
        entry.setFree(false);
        final int idx = findNextFree(stackNumber);
        if(idx == -1)
            throw new IllegalAccessError();
        this.data[idx] = entry;
        stacks[stackNumber-1].increaseSize();
        stacks[stackNumber-1].setLastUpdatedLocation(idx);
    }

    private int findNextFree(int stackNumber) {
        int lastUpdatedLocation = stacks[stackNumber-1].getLastUpdatedLocation();
        for(int i = lastUpdatedLocation ; i< data.length;i++){
            if(data[i].isFree())
               return i;
        }
        return -1;
    }

    public Object pop(int stackNumber){
        if(stacks[stackNumber-1].size == 0)
            throw new IllegalAccessError();
        int lastUpdatedLocation = stacks[stackNumber-1].getLastUpdatedLocation();
        Entry entry = data[lastUpdatedLocation];
        Entry newEntry = new Entry();
        data[lastUpdatedLocation] = newEntry;
        updateStack(lastUpdatedLocation,stackNumber);
        return entry.getValue();
    }

    private void updateStack(int lastUpdatedLocation, int stackNumber) {
        stacks[stackNumber-1].decreaseSize();
        for(int i = lastUpdatedLocation; i >= 0; i--){
            if(data[i].getStackNumber() == stackNumber){
                stacks[stackNumber-1].setLastUpdatedLocation(i);
                break;
            }
        }
    }


    private static class Entry {

        private int stackNumber = -1;

        Entry() {
            isFree = true;
        }

        public void setFree(boolean free) {
            isFree = free;
        }

        private boolean isFree;

        private boolean isFree(){
            return isFree;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
            this.isFree = false;
        }

        private Object value = null;

        public int getStackNumber() {
            return stackNumber;
        }

        public void setStackNumber(int stackNumber) {
            this.stackNumber = stackNumber;
        }
    }

    private static class XStack {

        public void increaseSize(){
            this.size++;
        }

        public void decreaseSize(){
            this.size--;
        }

        public void setLastUpdatedLocation(int lastUpdatedLocation) {
            this.lastUpdatedLocation = lastUpdatedLocation;
        }

        public int getLastUpdatedLocation() {
            return lastUpdatedLocation;
        }

        private int lastUpdatedLocation;

        public int getSize() {
            return size;
        }

        private int size = 0;

    }
}
